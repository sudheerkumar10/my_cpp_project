#include<iostream>
using namespace std;
//cont member function

class demo
{
    private:
    int num;
    const int c;
    public:
    /*if we want function like claculating result and average we dont want any modification in 
    th funtion so we use const function*/
    demo():num(20),c(30){} //mmbr inilzer list
    
    void disp() const //const member fun
    {
        cout<<"Num = "<<num<<endl;
        cout<<"Const C = "<<c<<endl;
        //this->num++; here we get error coz we have a const mmbr fun and we cant change any variable
                       /*inside a constant function for both const and non const values*/

        cout<<"after modification = "<<this->num<<endl;
    }
};

int main(void)
{
    demo d;
    d.disp();
    return 0;
}