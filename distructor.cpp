/*********************
Distrucotr
29-August-2020
*********************/

#include<iostream>
using namespace std;

class demo
{
    private:
    int n1,n2;
    public:
    demo():n1(30),n2(20) {}
    demo(int n1,int n2)
    {
        this->n1=n1;
        this->n2=n2;
    }

    //distructor special member of class with same name of class with '~' sign
    ~demo()//there can be only one distructor in a class so no need of peramerterized distrucotr
    {
        cout<<"Inside Distructor"<<endl<<this<<endl; //distructor always work in opposite sequence to constructor calling sequence
    } //because this is stored in stack and in stack last comes first
    // here this keyword is used to print the address of objects
    void disp()
    {
        cout<<"N1="<<n1<<endl;
        cout<<"N2="<<n2<<endl;
    }
};

int main()
{
    demo d1;
    d1.disp();
    demo d2(100,544);
    d2.disp();
    cout<<"Address of d1 is = "<<&d1<<endl;
    cout<<"Address of d2 is = "<<&d2<<endl;

    return 0;
}